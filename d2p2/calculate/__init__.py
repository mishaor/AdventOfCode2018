from string import ascii_lowercase
from logging import basicConfig, info, DEBUG
from difflib import SequenceMatcher, get_close_matches

basicConfig(format='[%(asctime)s] (%(threadName)s) %(levelname)s: %(message)s', level=DEBUG)

def calculate(input_data):
    # define a letters dict (required so that the results won't be dubbed)
    letters = {}

    # start calculating!
    for id in input_data:
        # clone input data
        clone_data = input_data.copy()
        # remove our element
        clone_data.remove(id)
        # find a close match for ID
        matches = get_close_matches(id, clone_data, cutoff=0.95)
        if len(matches) > 0:
            info('Close matches for \'{0}\': {1}'.format(id, matches))
        # pass if no matches
        else:
            continue

        # create a match string
        letter_match = ''
        
        # now we should have a more suitable and limited dataset to work with.
        for match in matches:
            # create a sequence matcher
            matcher = SequenceMatcher(None, id, match)
            # get a list of opcodes
            ops = matcher.get_opcodes()
            # iterate over opcodes
            for tag, a1, a2, b1, b2 in ops:
                if tag != 'equal':
                    continue
                letter_match += id[a1:a2]

        # add a match string to a letters dict
        letters[id] = letter_match

    # return the result
    return list(letters.values())[0]

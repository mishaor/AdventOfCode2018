from argparse import ArgumentParser
from pathlib import Path
import logging
from operator import add, sub

# create an argument parser
parser = ArgumentParser(description='Calibrates a device.')

# add an input file argument
parser.add_argument('filename', metavar='inputfile', type=str, help='path to file with input')

# parse arguments!
args = parser.parse_args()

# setup a logger
logging.basicConfig(format='[%(asctime)s] %(levelname)s: %(message)s', level=logging.DEBUG)

logging.info('Finished setting up the application.')

# create a path object and resolve it
path = Path(args.filename)
logging.info('Input file: {}'.format(path.resolve()))

# open the file and try to read it
input_file = open(path.resolve(), 'r')
file_contents = input_file.read()
logging.info('Opened the file.')
logging.info('File size: {}'.format(len(file_contents)))

# parse the newlines
lines = file_contents.splitlines()
logging.info('Parsed the file.')
logging.info('Entries: {}'.format(len(lines)))

# initialize counter
counter = 0
logging.info('Frequency starts at {}'.format(counter))

# calculate!
for calc in lines:
    mode_sym = calc[0] # get the mode symbol

    # parse it
    if mode_sym == '+':
        op = add
    if mode_sym == '-':
        op = sub

    num = int(calc[1:len(calc)]) # get the number

    new_counter = op(counter, num) # perform an operation

    logging.info('{0} {1} {2} = {3}'.format(counter, mode_sym, num, new_counter)) # log it!

    counter = new_counter

# print the result
logging.info('Done!')
logging.info('Frequency: {}'.format(counter))

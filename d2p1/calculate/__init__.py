from string import ascii_lowercase
from logging import basicConfig, info, DEBUG

basicConfig(format='[%(asctime)s] (%(threadName)s) %(levelname)s: %(message)s', level=DEBUG)

def calculate(input_data):
    # define all possible characters
    possible_characters = ascii_lowercase
    info('Possible characters: {}'.format(possible_characters))
    
    # initialize a matches dict
    matches = {
        'two': 0,
        'three': 0,
    }
    
    # start calculating!
    for id in input_data:
        two_found = False
        three_found = False

        for letter in possible_characters:
            letter_count = id.count(letter)
            
            if letter_count == 2:
                if not two_found:
                    two_found = True
            if letter_count == 3:
                if not three_found:
                    three_found = True
                
        if two_found:
            matches['two'] += 1
        if three_found:
            matches['three'] += 1
        
        info('Matches: {0} for 2 and {1} for 3'.format(matches['two'], matches['three']))
    
    # return a multiplied value
    return matches['two'] * matches['three']


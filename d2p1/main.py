from argparse import ArgumentParser
from pathlib import Path
from logging import info, basicConfig, DEBUG
from calculate import calculate

# create an argument parser
parser = ArgumentParser(description='Calibrates a device.')

# add an input file argument
parser.add_argument('filename', metavar='inputfile', type=str, help='path to file with input')

# parse arguments!
args = parser.parse_args()

# setup a logger
basicConfig(format='[%(asctime)s] (%(threadName)s) %(levelname)s: %(message)s', level=DEBUG)

info('Finished setting up the application.')

# create a path object and resolve it
path = Path(args.filename)
info('Input file: {}'.format(path.resolve()))

# open the file and try to read it
input_file = open(path.resolve(), 'r')
file_contents = input_file.read()
info('Opened the file.')
info('File size: {}'.format(len(file_contents)))

# parse the newlines
lines = file_contents.splitlines()
info('Parsed the file.')
info('Entries: {}'.format(len(lines)))

# start scanning
result = calculate(lines)

# print the result
info('Done!')
info('Result: {}'.format(result))
